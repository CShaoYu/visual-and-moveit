
#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <yolo_sub/ObjectCount.h>
#include <yolo_sub/BoundingBox.h>
#include <yolo_sub/BoundingBoxes.h>
#include <yolo_sub/ObjectCount.h>
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
//#include <message_filters/sync_policies/exact_time.h>  // 時間戳很難對上

//#define LABEL_NUMS 9
using namespace message_filters;
int catch_label = 0;

//volatile long int xc, yc;
//volatile float distance;

/*
typedef enum {
  cd,
  redbull,
  bluemountain,
  bernchon,
  cappuccino,
  olido,
  cup,
  cupear,
  coaster,
  tt
} CatchLabel;
typedef CatchLabel CatchLabel;
*/

enum CatchLabel
{
  cd,
  redbull,
  bluemountain,
  bernchon,
  cappuccino,
  olido,
  cup,
  cupear,
  coaster,
};

/*void depthCallback(const sensor_msgs::Image::ConstPtr& msg) {

    // Get a pointer to the depth values casting the data
    // pointer to floating point
    float* depths = (float*)(&msg->data[0]);

    // Image coordinates of the center pixel
    //int u = msg->width / 2;
    //int v = msg->height / 2;

    // Linear index of the center pixel
    int centerIdx = xc + msg->width * yc;
    distance = depths[centerIdx];

    // Output the measure
    //ROS_INFO("width %d, height %d, Center distance : %g m", msg->width, msg->height, depths[centerIdx]);
}*/

/*void yoloCallback(const yolo_sub::BoundingBoxes::ConstPtr& msg) {
    long int xmin = msg->bounding_boxes[0].xmin;
    long int ymin = msg->bounding_boxes[0].ymin;
    long int xmax = msg->bounding_boxes[0].xmax;
    long int ymax = msg->bounding_boxes[0].ymax;
    long int id = msg->bounding_boxes[0].id;
    xc = xmin + (xmax - xmin) / 2;
    yc = ymin + (ymax - ymin) / 2;
    ROS_INFO("%ld, %ld, %ld, %ld, %ld, %ld, %ld, %g", xmin, ymin, xmax, ymax, xc, yc, id, distance);

}*/

void callback(const sensor_msgs::Image::ConstPtr& msg1, const yolo_sub::BoundingBoxes::ConstPtr& msg2, const yolo_sub::ObjectCount::ConstPtr& msg3)
{
    int idx_tmp = -1;
    int size_tmp = msg3->count;
    /*for (int idx = 0; idx < LABEL_NUMS ; idx++)  //假設只有9個物體且無重複, 否則極大機率要寫同物體重新排列的算法ㄏ
      if (msg2->bounding_boxes[idx].id == catch_label)  
        idx_tmp = idx;    */
    /*
    ROS_INFO("%ld", sizeof(msg2));
    ROS_INFO("%ld", sizeof(*msg2));
    ROS_INFO("%ld", sizeof(&msg2));
    ROS_INFO("%ld", sizeof(msg2->bounding_boxes));
    ROS_INFO("%ld", sizeof(&msg2->bounding_boxes));
    ROS_INFO("%ld", sizeof(msg2->bounding_boxes[0]));
    ROS_INFO("%ld", sizeof(&msg2->bounding_boxes[0]));
    ROS_INFO("%ld", sizeof(msg2->bounding_boxes)/sizeof(&msg2->bounding_boxes[0]));
    */
    ros::param::get("catch_label", catch_label);  //放這才不會只執行一次
    CatchLabel catchlabel = static_cast<CatchLabel>(catch_label);  
    for (int idx = 0; idx < size_tmp ; idx++)  //目前假設只有9個物體且無重複, 否則極大機率要寫同物體重新排列的算法ㄏ
    {  if ((msg2->bounding_boxes[idx].id == catch_label)  &&  (&msg2->bounding_boxes[idx] != NULL))
       { 
         idx_tmp = idx;    
       }
    }
    ROS_INFO("Object detect %d", size_tmp);
    ROS_INFO("Catch idx %d", idx_tmp);
    if (idx_tmp >= 0)
    {
    float* depths = (float*)(&msg1->data[0]);  
    long int xmin = msg2->bounding_boxes[idx_tmp].xmin;  //[0]第一個偵測到的Label, 是隨機的
    long int ymin = msg2->bounding_boxes[idx_tmp].ymin; 
    long int xmax = msg2->bounding_boxes[idx_tmp].xmax;
    long int ymax = msg2->bounding_boxes[idx_tmp].ymax;
    long int id = msg2->bounding_boxes[idx_tmp].id;
    long int xc = xmin + (xmax - xmin) / 2;
    long int yc = ymin + (ymax - ymin) / 2;
    int centerIdx = xc + msg1->width * yc;
    float distance = depths[centerIdx];

    switch (catchlabel)
    {
      case cd:
        ROS_INFO("cd");
        break;
      case redbull:
        ROS_INFO("redbull");
        break;
      case bluemountain:
        ROS_INFO("bluemountain");
        break;
      case bernchon:
        ROS_INFO("bernchon");
        break;
      case cappuccino:
        ROS_INFO("cappuccino");
        break;
      case olido:
        ROS_INFO("olido");
        break;
      case cup:
        ROS_INFO("cup");
        break;
      case cupear:
        ROS_INFO("cupear");
        break;
      case coaster:
        ROS_INFO("coaster");
        break;
      default:
        break;
    }
    ROS_INFO("%ld, %ld, %ld, %ld, %ld, %ld, %ld, %g, %d", xmin, ymin, xmax, ymax, xc, yc, id, distance, catch_label);
    }
         

}

int main(int argc, char** argv) {

    ros::init(argc, argv, "yolo_sub_get_depth_node");
    ros::NodeHandle n;
    ros::param::set("catch_label", 0);

    //ros::Subscriber subDepth   = n.subscribe("/zed/zed_node/depth/depth_registered", 10, depthCallback);
    //ros::Subscriber subYolo    = n.subscribe("/darknet_ros/bounding_boxes", 10, yoloCallback);
    message_filters::Subscriber<sensor_msgs::Image> subDepth(n, "/zed/zed_node/depth/depth_registered", 1);
    message_filters::Subscriber<yolo_sub::BoundingBoxes> subYolo(n, "/darknet_ros/bounding_boxes", 1);
    message_filters::Subscriber<yolo_sub::ObjectCount> subYoloCnt(n, "/darknet_ros/found_object", 1);
    //ExactTime, ApproximateTime
    typedef sync_policies::ApproximateTime<sensor_msgs::Image, yolo_sub::BoundingBoxes, yolo_sub::ObjectCount> MySyncPolicy;
    // ApproximateTime takes a queue size as its constructor argument, hence MySyncPolicy(10)
    Synchronizer<MySyncPolicy> sync(MySyncPolicy(1000), subDepth, subYolo, subYoloCnt);
    sync.registerCallback(boost::bind(&callback, _1, _2, _3));
    //n.getParam("catch_label", catch_label);
    
    ros::spin();

    return 0;
}

#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <yolo_sub/ObjectCount.h>
#include <yolo_sub/BoundingBox.h>
#include <yolo_sub/BoundingBoxes.h>
#include <yolo_sub/ObjectCount.h>
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
//#include <message_filters/sync_policies/exact_time.h>  // 時間戳很難對上

using namespace message_filters;
int catch_label = 0;

enum CatchLabel
{
  cd,
  redbull,
  bluemountain,
  bernchon,
  cappuccino,
  olido,
  cup,
  cupear,
  coaster,
};

void callback(const sensor_msgs::Image::ConstPtr& msg1, 
              const yolo_sub::BoundingBoxes::ConstPtr& msg2,
              const yolo_sub::ObjectCount::ConstPtr& msg3)
{
    int idx_tmp = -1;  //沒初始值值會很奇怪可能很大的正數或是很大的負數
    int size_tmp = msg3->count;
    ros::param::get("catch_label", catch_label);  //放這才不會只執行一次
    CatchLabel catchlabel = static_cast<CatchLabel>(catch_label);  
    for (int idx = 0; idx < size_tmp ; idx++)  //目前假設只有9個物體且無重複, 否則極大機率要寫同物體重新排列的算法ㄏ
    {  if (msg2->bounding_boxes[idx].id == catch_label)
       { 
         idx_tmp = idx;    
       }
    }
    ROS_INFO("Object detect nums%d", size_tmp);
    ROS_INFO("Object detect idx %d", idx_tmp);   //[0]第一個偵測到的Label, 是隨機的
    if (idx_tmp >= 0)
    {
      float* depths = (float*)(&msg1->data[0]);  
      long int xmin = msg2->bounding_boxes[idx_tmp].xmin; 
      long int ymin = msg2->bounding_boxes[idx_tmp].ymin; 
      long int xmax = msg2->bounding_boxes[idx_tmp].xmax;
      long int ymax = msg2->bounding_boxes[idx_tmp].ymax;
      long int id = msg2->bounding_boxes[idx_tmp].id;
      long int xc = xmin + (xmax - xmin) / 2;
      long int yc = ymin + (ymax - ymin) / 2;
      int centerIdx = xc + msg1->width * yc;
      float distance = depths[centerIdx];
      
      switch (catchlabel)
      {
        case cd:
          ROS_INFO("cd");
          break;
        case redbull:
          ROS_INFO("redbull");
          break;
        case bluemountain:
          ROS_INFO("bluemountain");
          break;
        case bernchon:
          ROS_INFO("bernchon");
          break;
        case cappuccino:
          ROS_INFO("cappuccino");
          break;
        case olido:
          ROS_INFO("olido");
          break;
        case cup:
          ROS_INFO("cup");
          break;
        case cupear:
          ROS_INFO("cupear");
          break;
        case coaster:
          ROS_INFO("coaster");
          break;
        default:
          break;
      }
      ROS_INFO("%ld, %ld, %ld, %ld, %ld, %ld, %ld, %g, %d", xmin, ymin, xmax, ymax, xc, yc, id, distance, catch_label);
    }
}

int main(int argc, char** argv) {

    ros::init(argc, argv, "yolo_sub_get_depth_node");
    ros::NodeHandle n;
    ros::param::set("catch_label", 0);

    message_filters::Subscriber<sensor_msgs::Image> subDepth(n, "/zed/zed_node/depth/depth_registered", 1);
    message_filters::Subscriber<yolo_sub::BoundingBoxes> subYolo(n, "/darknet_ros/bounding_boxes", 1);
    message_filters::Subscriber<yolo_sub::ObjectCount> subYoloCnt(n, "/darknet_ros/found_object", 1);
    //ExactTime, ApproximateTime
    typedef sync_policies::ApproximateTime<sensor_msgs::Image, yolo_sub::BoundingBoxes, yolo_sub::ObjectCount> MySyncPolicy;
    // ApproximateTime takes a queue size as its constructor argument, hence MySyncPolicy(10)
    Synchronizer<MySyncPolicy> sync(MySyncPolicy(1000), subDepth, subYolo, subYoloCnt);
    sync.registerCallback(boost::bind(&callback, _1, _2, _3));
    //n.getParam("catch_label", catch_label);  //放這只會更新一次
    
    ros::spin();

    return 0;
}
